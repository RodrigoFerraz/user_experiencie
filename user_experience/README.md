Nome: Rodrigo Ferraz Barbosa

Atividade: Desenvolvedor Front-End

Introdução:

    Tendo em vista a necessidade de organização das viagens de finais de semana, desenvolvi o aplicativo ‘De férias com a maida.health’, o mesmo possibilita que o usuário faça uma busca por qualquer CEP do Brasil e do Mundo, o resultado é um card com as informações bairro, logadouro, cidade e estado.

Relação técnica:

    Para a parte de login e registro de usuário eu desenvolvi uma fakeAPI em nodeJS e subi no heroku.com; em relação aos dados do CEP utilizei a API disponibilizada no  viacep.com.br.

Links:

- Aplicação: https://maida-health-app.vercel.app/

- Repositório: https://gitlab.com/RodrigoFerraz/user_experiencie

- FakeAPI: https://gitlab.com/RodrigoFerraz/back-end_maida.health

- Heroku: https://dashboard.heroku.com/apps/maidahealth

- Figma: https://www.figma.com/file/7cG11RsgCi2V0rOtd1gqSB/Untitled?node-id=0%3A1

Bibliotecas React utilizadas:

– react-toastify
– react-hook-form
– @hookform/resolvers
– yup
– styled-components (mobile first)
– material-ui
– react-icons
– react-router-dom
– axios (baseURL)
– context API

Agradecimentos:

    Agradeço a Lavinia Brandão por ter me escolhido para esta oportunidade.
