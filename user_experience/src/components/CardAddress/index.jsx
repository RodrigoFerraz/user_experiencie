import { useRequiredAddress } from "../../Providers/RequiredAddress";
import { Container, ContainerInfo, ContainerFooter } from "./style";
import LocationCityIcon from "@material-ui/icons/LocationCity";
import ImageAspectRatioIcon from "@material-ui/icons/ImageAspectRatio";
import ApartmentIcon from "@material-ui/icons/Apartment";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import { Button } from "@material-ui/core";
import { useEffect } from "react";
import { toast } from "react-toastify";
const CardAddress = () => {
  const { info, setFavorites, favorites, validated } = useRequiredAddress();
  const handleFavorites = () => {
    if (favorites.find((item) => item.cep === info.cep)) {
      toast.error("Já adicionado aos favoritos!");
    } else {
      setFavorites([...favorites, info]);
      toast.success("Adicionado aos favoritos ;)");
    }
  };

  useEffect(() => {
    localStorage.setItem("@MaidaHealth:favorites", JSON.stringify(favorites));
  }, [favorites]);

  const styledIcons = { width: "17px", heigth: "20px" };

  return (
    <>
      {validated && (
        <Container>
          <ContainerInfo>
            {" "}
            <ImageAspectRatioIcon
              color="secondary"
              size="medium"
              style={styledIcons}
            />{" "}
            Bairro: {info.bairro}
          </ContainerInfo>
          <ContainerInfo>
            {" "}
            <ApartmentIcon
              color="secondary"
              size="medium"
              style={styledIcons}
            />{" "}
            Logadouro: {info?.logradouro?.slice(0, 20) + "..."}
          </ContainerInfo>
          <ContainerInfo>
            {" "}
            <LocationCityIcon
              color="secondary"
              size="medium"
              style={styledIcons}
            />{" "}
            Localidade: {info.localidade}
          </ContainerInfo>
          <ContainerInfo>
            {" "}
            <LocationOnIcon
              color="secondary"
              size="medium"
              style={styledIcons}
            />{" "}
            Estado: {info.uf}
          </ContainerInfo>
          <ContainerFooter>
            <Button
              style={{ margin: "7px" }}
              variant="contained"
              color="primary"
              onClick={handleFavorites}
            >
              Favoritar{" "}
            </Button>
          </ContainerFooter>
        </Container>
      )}
    </>
  );
};
export default CardAddress;
