import "./style.css";
const ButtonDarkMode = () => {
  return (
    <>
      <label>
        <input type="checkbox"></input>
        <span className="check"></span>
      </label>
    </>
  );
};

export default ButtonDarkMode;
