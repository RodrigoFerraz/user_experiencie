import styled from "styled-components";

export const FooterButton = styled.button`
  border-style: none;
  width: 274px;
  height: 56px;
  background-color: #3f51b5;
  color: #fff;
  border-radius: 8px;
  font-size: 1.5rem;
  letter-spacing: 1px;
  @media (min-width: 1024px) {
    width: 423px;
    height: 87px;
    cursor: pointer;
  }
`;

export const Container = styled.div`
  input {
    background: #fff;
    width: 236px;
    @media (min-width: 1024px) {
      width: 360px;
      height: 43px;
      font-size: 1.5rem;
    }
  }
`;
