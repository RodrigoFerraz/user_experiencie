import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useRequiredAddress } from "../../Providers/RequiredAddress";
import { TextField } from "@material-ui/core";
import { FooterButton, Container } from "./style";

const FormStore = () => {
  const { getInfo, setValidated } = useRequiredAddress();
  const schema = yup.object().shape({
    CEP: yup
      .string()
      .required("Digite um CEP válido")
      .min(8, "CEP deve possuir 8 números"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({ resolver: yupResolver(schema) });

  const handleInformation = (data) => {
    const { CEP } = data;
    getInfo(CEP);
    reset();
    setValidated(true);
  };

  return (
    <Container>
      <form onSubmit={handleSubmit(handleInformation)}>
        <TextField
          required
          margin="normal"
          variant="filled"
          label="Digite seu CEP"
          size="small"
          color="primary"
          {...register("CEP")}
          error={!!errors.password}
          helperText={errors.password?.message}
        ></TextField>
        <p>{errors.CEP?.message}</p>
        <FooterButton type="submit">Procurar</FooterButton>
      </form>
    </Container>
  );
};

export default FormStore;
