import styled from "styled-components";

export const FooterButton = styled.button`
  border-style: none;
  width: 274px;
  height: 56px;
  background-color: #8257e5;
  color: #fff;
  border-radius: 8px;
  font-size: 1.5rem;
  letter-spacing: 1px;
  margin-top: 27px;
  @media (min-width: 1024px) {
    width: 423px;
    height: 87px;
    cursor: pointer;
  }
`;

export const Container = styled.div`
  input {
    background: #fff;
    width: 236px;
    @media (min-width: 1024px) {
      width: 360px;
      height: 43px;
      font-size: 1.5rem;
    }
  }
`;

export const ContainerLoading = styled.div`
  img {
    width: 47px;
    height: 47px;
    border-radius: 100%;
  }
  h2 {
    font-size: 1rem;
    margin: 0px;
  }
`;
