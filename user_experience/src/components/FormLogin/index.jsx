import { TextField } from "@material-ui/core";
import { FooterButton, Container, ContainerLoading } from "./style";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useHistory } from "react-router-dom";
import api from "../../services";
import { toast } from "react-toastify";
import { useRequiredAddress } from "../../Providers/RequiredAddress";
import AvatarLoading from "../../assets/loading_maidaHealth.gif";
import { useInfoUsers } from "../../Providers/InfoUsers";
import jwt_decode from "jwt-decode";
import { useEffect } from "react";

const FormLogin = () => {
  const history = useHistory();
  const { setIsLoading, isLoading } = useRequiredAddress();
  const { token, setToken, id, setId, getUserInfo, infoUsers } = useInfoUsers();
  useEffect(() => {
    getUserInfo(id, token);
  }, [token]);

  const schema = yup.object().shape({
    email: yup.string().email("Email inválido").required("Nome inválido"),
    password: yup.string().min(9, "minimo de 9 dígitos"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({ resolver: yupResolver(schema) });
  const handleForm = (data) => {
    setIsLoading(true);
    api
      .post("login/", data)
      .then((response) => {
        const validToken = response.data.accessToken;
        const decoded = jwt_decode(validToken);
        setId(decoded.sub);
        setToken(validToken);
        localStorage.setItem("@maidaHealth:token", JSON.stringify(validToken));
        reset();
        history.push("/store");
        setIsLoading(false);
      })
      .catch((e) => {
        setIsLoading(false);
        toast.error("Email ou Senha inválido!");
      });
  };
  return (
    <form onSubmit={handleSubmit(handleForm)}>
      <Container>
        {isLoading && (
          <ContainerLoading>
            <img src={AvatarLoading} alt="Loading_Login"></img>
            <h2>Carregando...</h2>
          </ContainerLoading>
        )}
        <TextField
          required
          margin="normal"
          variant="filled"
          label="Email"
          size="small"
          color="primary"
          {...register("email")}
          error={!!errors.email}
          helperText={errors.email?.message}
        ></TextField>
      </Container>
      <Container>
        <TextField
          required
          margin="normal"
          variant="filled"
          label="Senha"
          size="small"
          color="primary"
          type="password"
          {...register("password")}
          error={!!errors.password}
          helperText={errors.password?.message}
        ></TextField>
      </Container>
      <footer>
        <FooterButton type="submit">Entrar</FooterButton>
      </footer>
    </form>
  );
};
export default FormLogin;
