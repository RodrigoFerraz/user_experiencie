import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: baseline;
  svg {
    position: relative;
    top: 0.4rem;
  }
  box-shadow: 0px 2px 5px 2px;
  background-color: #8257e5;
  width: 300px;
  margin-bottom: 13px;
  margin-top: 13px;
  @media (min-width: 1024px) {
    width: 424px;
  }
`;

export const ContainerInfo = styled.h1`
  margin: 0px;
  font-size: 1rem;
`;

export const FooterButton = styled.button`
  border-style: none;
  width: 274px;
  height: 56px;
  background-color: #8257e5;
  color: #fff;
  border-radius: 8px;
  font-size: 1.5rem;
  letter-spacing: 1px;
`;

export const ContainerFooter = styled.footer`
  display: flex;
  flex-direction: row;
  justify-content: center;
  width: 100%;
`;
