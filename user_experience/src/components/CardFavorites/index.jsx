import { useRequiredAddress } from "../../Providers/RequiredAddress";
import { Container, ContainerInfo, ContainerFooter } from "./style";
import LocationCityIcon from "@material-ui/icons/LocationCity";
import ImageAspectRatioIcon from "@material-ui/icons/ImageAspectRatio";
import ApartmentIcon from "@material-ui/icons/Apartment";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import { Button } from "@material-ui/core";
import { toast } from "react-toastify";

const CardFavorites = ({ element }) => {
  const { setFavorites, favorites } = useRequiredAddress();
  const handleRemoveFavorites = () => {
    setFavorites(favorites.filter((item) => item.cep !== element.cep));
    toast.success("Removido dos favoritos!");
    localStorage.removeItem("@MaidaHealth:favorites");
  };

  const styledIcons = { width: "17px", heigth: "20px" };

  return (
    <>
      <Container>
        <ContainerInfo>
          {" "}
          <ImageAspectRatioIcon
            color="secondary"
            size="medium"
            style={styledIcons}
          />{" "}
          Bairro: {element.bairro}
        </ContainerInfo>
        <ContainerInfo>
          {" "}
          <ApartmentIcon
            color="secondary"
            size="medium"
            style={styledIcons}
          />{" "}
          Logradouro: {element.logradouro?.slice(0, 20) + "..."}
        </ContainerInfo>
        <ContainerInfo>
          {" "}
          <LocationCityIcon
            color="secondary"
            size="medium"
            style={styledIcons}
          />{" "}
          Cidade: {element.localidade}
        </ContainerInfo>
        <ContainerInfo>
          {" "}
          <LocationOnIcon
            color="secondary"
            size="medium"
            style={styledIcons}
          />{" "}
          Estado: {element.uf}
        </ContainerInfo>
        <ContainerFooter>
          <Button
            style={{ margin: "7px" }}
            variant="contained"
            color="primary"
            onClick={handleRemoveFavorites}
          >
            Remover{" "}
          </Button>
        </ContainerFooter>
      </Container>
    </>
  );
};
export default CardFavorites;
