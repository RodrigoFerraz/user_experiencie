import { TextField } from "@material-ui/core";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useHistory } from "react-router-dom";
import api from "../../services";
import { FooterButton, Container, ContainerLoading } from "./style";
import { toast } from "react-toastify";
import { useRequiredAddress } from "../../Providers/RequiredAddress";
import AvatarLoading from "../../assets/loading_maidaHealth.gif";

const FormRegister = () => {
  const history = useHistory();
  const { isLoading, setIsLoading } = useRequiredAddress();

  const schema = yup.object().shape({
    username: yup.string().required("Campo é obrigatório"),
    password: yup
      .string()
      .min(9, "Minimo de 9 digitos")
      .required("Campo é obrigatório"),
    email: yup.string().email("Email inválido").required("Campo obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({ resolver: yupResolver(schema) });

  const handleForm = (data) => {
    const { username, password, email } = data;
    const newData = { username, password, email, favorites: {} };
    setIsLoading(true);
    api
      .post("users/", newData)
      .then((response) => {
        reset();
        history.push("/login");
        toast.success("Cadastrado com sucesso!");
        setIsLoading(false);
      })
      .catch((e) => {
        setIsLoading(false);
        toast.error("Usuário ou Email já cadastrado!");
      });
  };

  return (
    <form onSubmit={handleSubmit(handleForm)}>
      <Container>
        {isLoading && (
          <ContainerLoading>
            <img src={AvatarLoading} alt="Loading_Register"></img>
            <h2>Carregando...</h2>
          </ContainerLoading>
        )}
        <TextField
          required
          margin="normal"
          variant="filled"
          label="Usuário"
          size="small"
          color="primary"
          {...register("username")}
          error={!!errors.username}
          helperText={errors.username?.message}
        ></TextField>
      </Container>
      <Container>
        <TextField
          required
          margin="normal"
          variant="filled"
          label="Email"
          size="small"
          color="primary"
          {...register("email")}
          error={!!errors.email}
          helperText={errors.email?.message}
        ></TextField>
      </Container>
      <Container>
        <TextField
          required
          margin="normal"
          variant="filled"
          label="Senha"
          size="small"
          color="primary"
          type="password"
          {...register("password")}
          error={!!errors.password}
          helperText={errors.password?.message}
        ></TextField>
      </Container>
      <footer>
        <FooterButton type="submit">Registrar</FooterButton>
      </footer>
    </form>
  );
};

export default FormRegister;
