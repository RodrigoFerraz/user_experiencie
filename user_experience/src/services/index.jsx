import axios from "axios";
const api = axios.create({
  baseURL: "https://maidahealth.herokuapp.com/",
});

export default api;
