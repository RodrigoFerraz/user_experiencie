import axios from "axios";
const apiInfo = axios.create({
  baseURL: "https://viacep.com.br/ws/",
});

export default apiInfo;
