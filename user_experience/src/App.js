import "./App.css";
import Routes from "./routes";
import { Toast } from "./components/Toastify";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Toast></Toast>
        <Routes></Routes>
      </header>
    </div>
  );
}

export default App;
