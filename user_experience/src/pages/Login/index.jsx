import AvatarLogin from "../../assets/Login.svg";
import FormLogin from "../../components/FormLogin";
import { Link, useHistory, Redirect } from "react-router-dom";
import { Button } from "@material-ui/core";
import { ContainerImage, Container, ContainerAside } from "./style";
import HomeIcon from "@material-ui/icons/Home";
import { useInfoUsers } from "../../Providers/InfoUsers";
const Login = () => {
  const history = useHistory();
  const { token } = useInfoUsers();

  const style = {
    position: "absolute",
    top: "1rem",
    left: "1rem",
    background: "#6c63ff",
    color: "white",
  };

  if (token) {
    return <Redirect to="/store" />;
  }

  return (
    <Container>
      <Button
        onClick={() => history.push("/")}
        style={style}
        startIcon={<HomeIcon />}
        variant="contained"
      >
        {" "}
        Início
      </Button>
      <ContainerImage src={AvatarLogin}></ContainerImage>
      <ContainerAside>
        <FormLogin></FormLogin>
        <p>
          Não possui uma conta?{" "}
          <Link style={{ color: "#6c63ff" }} to="/register">
            registre-se
          </Link>
        </p>
      </ContainerAside>
    </Container>
  );
};

export default Login;
