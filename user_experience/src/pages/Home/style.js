import styled from "styled-components";

export const Container = styled.div`
  height: 100vh;
  font-family: "Lato", sans-serif;
  @media (min-width: 1024px) {
    display: flex;
    flex-direction: row;
    align-items: center;
  }
`;

export const ContainerImage = styled.img`
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  height: 35%;
  width: 100%;
  @media (min-width: 768px) {
    width: 53%;
  }
  @media (min-width: 1024px) {
    width: 43%;
  }
`;

export const ContainerTitle = styled.h1`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  font-size: 2.1rem;
  .firstSpan {
    color: #0095f6;
  }
  .secondSpan {
    color: pink;
  }
  .thirdSpan {
    color: #c51ea8;
  }
`;

export const FooterButton = styled.button`
  border-style: none;
  width: 274px;
  height: 56px;
  background-color: #8257e5;
  color: #fff;
  border-radius: 8px;
  font-size: 1.5rem;
  letter-spacing: 1px;
  @media (min-width: 1024px) {
    width: 423px;
    height: 87px;
    cursor: pointer;
  }
`;

export const FooterButtonBlue = styled(FooterButton)`
  background-color: #6c63ff;
`;

export const FooterContainer = styled.footer`
  height: 156px;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  margin-top: 20px;
  @media (min-width: 1024px) {
    width: 50vw;
    height: 357px;
  }
`;

export const ContainerHeader = styled.header`
  @media (min-width: 1024px) {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-evenly;
    background-color: #282c34;
    height: 100%;
    width: 100%;
  }
`;
