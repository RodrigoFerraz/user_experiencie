import { useHistory } from "react-router-dom";
import {
  Container,
  ContainerImage,
  FooterButton,
  FooterContainer,
  ContainerTitle,
  FooterButtonBlue,
  ContainerHeader,
} from "./style";
import AvatarLogin from "../../assets/Home.svg";

const Home = () => {
  const history = useHistory();

  const handleAuthentication = (value) => {
    history.push(value);
  };

  return (
    <Container>
      <ContainerHeader>
        <ContainerTitle>
          <span className="firstSpan">M</span>AIDA
          <span className="secondSpan">.</span>
          HEALT<span className="thirdSpan">H</span>
        </ContainerTitle>
        <ContainerImage src={AvatarLogin}></ContainerImage>
        <p style={{ marginTop: "23px" }}>
          Sabendo a necessidade da organização das viagens de finais de semana,
          criamos a <strong>De férias com a maida.health</strong>{" "}
        </p>
      </ContainerHeader>
      <FooterContainer>
        <FooterButton onClick={() => handleAuthentication("/login")}>
          Login
        </FooterButton>
        <FooterButtonBlue onClick={() => handleAuthentication("/register")}>
          Register
        </FooterButtonBlue>
      </FooterContainer>
    </Container>
  );
};
export default Home;
