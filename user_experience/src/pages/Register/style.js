import styled from "styled-components";

export const ContainerImage = styled.img`
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  height: 35%;
  width: 80%;
  margin-top: 23px;
  @media (min-width: 768px) {
    width: 45%;
    cursor: pointer;
  }
`;

export const ContainerAside = styled.div`
  @media (min-width: 1024px) {
    display: flex;
    flex-direction: column;
  }
`;

export const Container = styled.div`
  @media (min-width: 1024px) {
    display: flex;
    flex-direction: row;
    width: 100%;
    align-items: center;
    justify-content: space-evenly;
  }
`;
