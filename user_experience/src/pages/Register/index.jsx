import FormRegister from "../../components/FormRegister";
import { Link, useHistory, Redirect } from "react-router-dom";
import AvatarRegister from "../../assets/Register.svg";
import { ContainerImage, Container, ContainerAside } from "./style";
import HomeIcon from "@material-ui/icons/Home";
import { Button } from "@material-ui/core";
import { useInfoUsers } from "../../Providers/InfoUsers";

const Register = () => {
  const { token } = useInfoUsers();
  const history = useHistory();
  const style = {
    position: "absolute",
    top: "1rem",
    left: "1rem",
    background: "#6c63ff",
    color: "white",
  };

  if (token) {
    return <Redirect to="/store" />;
  }

  return (
    <Container>
      <Button
        onClick={() => history.push("/")}
        style={style}
        startIcon={<HomeIcon />}
        variant="contained"
      >
        {" "}
        Início
      </Button>
      <ContainerImage src={AvatarRegister}></ContainerImage>
      <ContainerAside>
        <FormRegister></FormRegister>
        <p>
          Já possui uma conta?{" "}
          <Link style={{ color: "#6c63ff" }} to="/login">
            Entrar
          </Link>
        </p>
      </ContainerAside>
    </Container>
  );
};
export default Register;
