import styled from "styled-components";

export const ContainerImage = styled.img`
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  height: 35%;
  width: 57%;
  margin-top: 13px;
  @media (min-width: 768px) {
    width: 43%;
  }
  @media (min-width: 1024px) {
    width: 34%;
  }
`;

export const ContainerAside = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  @media (min-width: 1024px) {
    display: flex;
    flex-direction: column;
    height: 573px;
    justify-content: space-evenly;
  }
`;

export const Container = styled.div`
  @media (min-width: 1024px) {
    display: flex;
    flex-direction: row;
    width: 100%;
    align-items: center;
    justify-content: space-evenly;
  }
  .buttonExit {
    position: absolute;
    top: 1rem;
    left: 1rem;
    @media (min-width: 1024px) {
      width: 137px;
      height: 47px;
    }
  }
  .buttonFavorites {
    position: absolute;
    top: 1rem;
    right: 1rem;
    @media (min-width: 1024px) {
      width: 177px;
      height: 47px;
    }
  }
`;
