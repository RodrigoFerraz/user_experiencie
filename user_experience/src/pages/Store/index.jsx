import FormStore from "../../components/FormStore";
import CardAddress from "../../components/CardAddress";
import AvatarStore from "../../assets/Store.svg";
import { ContainerImage, Container, ContainerAside } from "./style";
import { Button } from "@material-ui/core";
import MeetingRoomIcon from "@material-ui/icons/MeetingRoom";
import StarIcon from "@material-ui/icons/Star";
import { useHistory, Redirect } from "react-router-dom";
import { useRequiredAddress } from "../../Providers/RequiredAddress";
import { useInfoUsers } from "../../Providers/InfoUsers";
const Store = () => {
  const { token, setToken } = useInfoUsers();
  const history = useHistory();
  const { setUserAccess } = useRequiredAddress();

  const handleLogout = () => {
    localStorage.clear();
    setToken("");
    history.push("/");
    setUserAccess(false);
  };
  if (!token) {
    return <Redirect to="/login" />;
  }

  return (
    <Container>
      <Button
        className="buttonExit"
        onClick={handleLogout}
        startIcon={<MeetingRoomIcon />}
        variant="contained"
        color="secondary"
      >
        {" "}
      </Button>
      <Button
        className="buttonFavorites"
        onClick={() => history.push("/favorites")}
        endIcon={<StarIcon />}
        variant="contained"
        color="primary"
      >
        {" "}
        Favoritos
      </Button>
      <ContainerImage src={AvatarStore}></ContainerImage>
      <ContainerAside>
        <FormStore></FormStore>
        <CardAddress></CardAddress>
      </ContainerAside>
    </Container>
  );
};

export default Store;
