import styled from "styled-components";

export const ContainerImage = styled.img`
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  height: 35%;
  width: 80%;
  margin-top: 23px;
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
  height: 100vh;
`;

export const ContainerFavoritesCards = styled.div`
  height: 60vh;
  overflow-y: scroll;
  width: 323px;
  display: flex;
  flex-direction: column;
  align-items: center;
  @media (min-width: 1024px) {
    height: 42vh;
    width: 74vh;
    justify-content: space-evenly;
    overflow-x: auto;
    flex-direction: row;
    flex-wrap: wrap;
  }
`;
