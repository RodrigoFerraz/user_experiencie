import CardFavorites from "../../components/CardFavorites";
import { useRequiredAddress } from "../../Providers/RequiredAddress";
import AvatarFavorites from "../../assets/Favorites.svg";
import { ContainerImage, Container, ContainerFavoritesCards } from "./style";
import { Button } from "@material-ui/core";
import { useHistory, Redirect } from "react-router-dom";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import { useInfoUsers } from "../../Providers/InfoUsers";

const Favorites = () => {
  const { favorites } = useRequiredAddress();
  const { token } = useInfoUsers();

  const history = useHistory();

  if (!token) {
    return <Redirect to="/login" />;
  }

  return (
    <Container>
      <Button
        onClick={() => history.goBack()}
        style={{ position: "absolute", top: "1rem", left: "1rem" }}
        startIcon={<ArrowBackIosIcon />}
        variant="contained"
        color="primary"
      >
        {" "}
        Voltar
      </Button>

      <ContainerImage src={AvatarFavorites}></ContainerImage>
      <h2>Meus Favoritos</h2>
      <ContainerFavoritesCards>
        {favorites.map((element, index) => (
          <CardFavorites element={element} key={index}></CardFavorites>
        ))}
      </ContainerFavoritesCards>
    </Container>
  );
};

export default Favorites;
