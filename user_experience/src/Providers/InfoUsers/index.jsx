import { createContext, useContext, useState } from "react";
import api from "../../services";
import { toast } from "react-toastify";

export const InfoUsersContext = createContext();

export const InfoUsersProvider = ({ children }) => {
  const [infoUsers, setInfoUsers] = useState({});
  const [id, setId] = useState("");
  const [token, setToken] = useState(() => {
    return JSON.parse(localStorage.getItem("@maidaHealth:token")) || "";
  });

  const [favorites, setFavorites] = useState(() => {
    return JSON.parse(localStorage.getItem("@maidaHealth:favorites")) || [];
  });

  const getUserInfo = (id, token) => {
    api.get(`/users/${id}/`).then((res) => {
      localStorage.setItem("@maidaHealth:userInfo", JSON.stringify(res.data));
      setInfoUsers(res.dataAccess);
    });
  };

  return (
    <InfoUsersContext.Provider
      value={{
        infoUsers,
        setInfoUsers,
        setToken,
        token,
        id,
        setId,
        getUserInfo,
      }}
    >
      {children}
    </InfoUsersContext.Provider>
  );
};

export const useInfoUsers = () => useContext(InfoUsersContext);
