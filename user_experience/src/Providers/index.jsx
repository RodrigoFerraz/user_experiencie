import { RequiredAddressProvider } from "./RequiredAddress";
import { InfoUsersProvider } from "./InfoUsers";

const Providers = ({ children }) => {
  return (
    <RequiredAddressProvider>
      <InfoUsersProvider>{children}</InfoUsersProvider>
    </RequiredAddressProvider>
  );
};

export default Providers;
