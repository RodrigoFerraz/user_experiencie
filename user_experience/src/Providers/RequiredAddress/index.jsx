import { createContext, useContext, useState } from "react";
import apiInfo from "../../services/servicesInfo";
import { toast } from "react-toastify";

export const RequiredAddressContext = createContext();

export const RequiredAddressProvider = ({ children }) => {
  const [info, setInfo] = useState({});
  const [favorites, setFavorites] = useState(() => {
    return JSON.parse(localStorage.getItem("@MaidaHealth:favorites")) || [];
  });
  const [validated, setValidated] = useState(false);
  const [userAccess, setUserAccess] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const getInfo = (CEP) => {
    apiInfo
      .get(`${CEP}/json/`)
      .then((response) => {
        setInfo(response.data);
      })
      .catch((e) =>
        toast.error("CEP inválido", {
          autoClose: 1500,
          hideProgressBar: true,
          closeOnClick: true,
        })
      );
  };

  return (
    <RequiredAddressContext.Provider
      value={{
        getInfo,
        info,
        setFavorites,
        favorites,
        setValidated,
        validated,
        userAccess,
        setUserAccess,
        isLoading,
        setIsLoading,
      }}
    >
      {children}
    </RequiredAddressContext.Provider>
  );
};

export const useRequiredAddress = () => useContext(RequiredAddressContext);
