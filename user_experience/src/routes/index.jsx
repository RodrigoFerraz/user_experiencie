import { Switch, Route } from "react-router-dom";
import Login from "../pages/Login";
import Register from "../pages/Register";
import Home from "../pages/Home";
import Store from "../pages/Store";
import NotFound from "../pages/NotFound";
import Favorites from "../pages/Favorites";
const Routes = () => {
  return (
    <>
      <Switch>
        <Route path="/store">
          <Store></Store>
        </Route>
        <Route path="/favorites">
          <Favorites></Favorites>
        </Route>
        <Route exact path="/">
          <Home></Home>
        </Route>
        <Route path="/login">
          <Login></Login>
        </Route>
        <Route path="/register">
          <Register></Register>
        </Route>
        <Route path="*">
          <NotFound></NotFound>
        </Route>
      </Switch>
    </>
  );
};

export default Routes;
